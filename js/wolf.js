const $ = document

let wolf_container = $.querySelector(".wolf-container")
let wolf_id = parseInt(window.location.href.split("?id=")[1]) 

const fetchDestroy = () => {
    let fetchConfig ={
        method:"DELETE"
    }
    fetch(api+wolf_id, fetchConfig)
    .then(resp => {
        alert("Lobo excluido")
        window.location.href = "list_wolves.html"
    })
}


fetch(api+wolf_id)
.then(resp => resp.json())
.then(resp => {
    wolf_container.innerHTML=`
        <h3>${resp.name}</h3>
        <div>
            <div class="wolf-display-image">
                <img src=${resp.link_image} class="wolf-img">
                <div>
                </div>
            </div>
            <p>${resp.description}</p>
        </div>
    `
})