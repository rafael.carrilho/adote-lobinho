const $ = document

let addwolf_form = $.querySelector("#addwolf-form")

addwolf_form.addEventListener("submit", e => {
    e.preventDefault()
    let name = $.querySelector("#wolf-name").value
    let age = $.querySelector("#wolf-age").value
    let photo = $.querySelector("#wolf-photo").value
    let desc = $.querySelector("#wolf-description").value

    let fetchBody = {
        "wolf":{
            "name": name,
            "age": age,
            "link_image": photo,
            "description": desc
        }
    }
    let fetchConfig = {
        method: "POST",
        headers:{"Content-Type": "application/json"},
        body: JSON.stringify(fetchBody)
    }
    fetch(api, fetchConfig)
    .then(resp => resp.json())
    .then(resp => {
        alert("Lobo criado")
        window.location.href = "list_wolves.html"
    })
})
