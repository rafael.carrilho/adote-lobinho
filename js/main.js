const api = "https://lobinhos.herokuapp.com/wolves/"
const fetchHeader = {
    "Content-Type": "application/JSON"
}

const createWolfCard = ($, wolf) => {
    let cardWolf = $.createElement('div')
    cardWolf.innerHTML = `
        <div class="image-div-back"> 
            <img class="image-div" src=${wolf.link_image} alt="Imagem do lobo">
        </div>
        <div>
            <div class="row-first-wolf">
                <div>
                    <h3>${wolf.name}</h3>
                    <p class="age-subtitle">Idade: ${wolf.age} ano${wolf.age > 1? "s":""}</p>
                </div>
                <button class="button-main" onclick="window.location.href = 'wolf.html?id=${wolf.id}' "> Adotar </button>
            </div>
            <p>${wolf.description}</p>    
        </div>
    `
    cardWolf.classList.add('card-wolf-div')
    return cardWolf
}
window.addEventListener("scroll", e => { 
    let header = document.querySelector(".header-container")
    if(window.pageYOffset > 0){
        header.classList.add("header-active")
    }else{
        header.classList.remove("header-active")
    }
})