const $ = document;

let wolvesAll;
let pages;
let actual_page = 1;
const wolves_container = $.querySelector("#wolves-container");
const pagination_field = $.querySelector("#pagination-field")
const research_input = $.querySelector("#search-input")

research_input.addEventListener("change", e => {
    page = 1
    wolvesFiltered = wolvesAll.filter(element => (element.name == e.target.value))
    if (e.target.value == ""){
        wolvesFiltered = wolvesAll
    }
    wolves_container.innerHTML=""
    actual_page = page
    let downValue = (page - 1)*4 
    let upperValue = page*4 
    let wolvesInPage = wolvesFiltered.slice(downValue, upperValue)
    for (let wolf in wolvesInPage) {
        wolves_container.append(createWolfCard($, wolvesInPage[wolf]))
    }
    flowPagination(page)
})

const flowPagination = () => {
    if (actual_page==1){
        pagination_field.innerHTML=`
            <button onclick="flowPages(2)">2</button>
            <button onclick="flowPages(3)">3</button>
            <button onclick="flowPages(4)">4</button>
            <button onclick="flowPages(5)">5</button>
            <button onclick="flowPages(6)">6</button>
            <button onclick="flowPages(7)">7</button>
            <button onclick="flowPages(${actual_page+1})">>></button>
        `
    }else if(actual_page==2){
        pagination_field.innerHTML=`
            <button onclick="flowPages(1)">1</button>
            <button onclick="flowPages(3)">3</button>
            <button onclick="flowPages(4)">4</button>
            <button onclick="flowPages(5)">5</button>
            <button onclick="flowPages(6)">6</button>
            <button onclick="flowPages(7)">7</button>
            <button onclick="flowPages(${actual_page+1})">>></button>
        `
    }else if(actual_page == 3){
        pagination_field.innerHTML=`
            <button onclick="flowPages(1)">1</button>
            <button onclick="flowPages(2)">2</button>
            <button onclick="flowPages(4)">4</button>
            <button onclick="flowPages(5)">5</button>
            <button onclick="flowPages(6)">6</button>
            <button onclick="flowPages(7)">7</button>
            <button onclick="flowPages(${actual_page+1})">>></button>
        `
    }else if(actual_page == pages){
        pagination_field.innerHTML=`
            <button onclick="flowPages(${actual_page-7})"><<</button>
            <button onclick="flowPages(${actual_page-6})">${actual_page-6}</button>
            <button onclick="flowPages(${actual_page-5})">${actual_page-5}</button>
            <button onclick="flowPages(${actual_page-4})">${actual_page-4}</button>
            <button onclick="flowPages(${actual_page-3})">${actual_page-3}</button>
            <button onclick="flowPages(${actual_page-2})">${actual_page-2}</button>
            <button onclick="flowPages(${actual_page-1})">${actual_page-1}</button>
        `
    }else if(actual_page == pages-1){
        pagination_field.innerHTML=`
            <button onclick="flowPages(${actual_page-6})"><<</button>
            <button onclick="flowPages(${actual_page-5})">${actual_page-5}</button>
            <button onclick="flowPages(${actual_page-4})">${actual_page-4}</button>
            <button onclick="flowPages(${actual_page-3})">${actual_page-3}</button>
            <button onclick="flowPages(${actual_page-2})">${actual_page-2}</button>
            <button onclick="flowPages(${actual_page-1})">${actual_page-1}</button>
            <button onclick="flowPages(${actual_page+1})">${actual_page+1}</button>
        `
    }else if(actual_page == pages-2){
        pagination_field.innerHTML=`
            <button onclick="flowPages(${actual_page-5})"><<</button>
            <button onclick="flowPages(${actual_page-4})">${actual_page-4}</button>
            <button onclick="flowPages(${actual_page-3})">${actual_page-3}</button>
            <button onclick="flowPages(${actual_page-2})">${actual_page-2}</button>
            <button onclick="flowPages(${actual_page-1})">${actual_page-1}</button>
            <button onclick="flowPages(${actual_page+1})">${actual_page+1}</button>
            <button onclick="flowPages(${actual_page+2})">${actual_page+2}</button>
        `
    }else{
        let pagination_list = [actual_page-3, actual_page-2, actual_page-1, actual_page+1, actual_page+2, actual_page+3]
        pagination_field.innerHTML = `<button onclick="flowPages(${actual_page-1})"><<</button>`
        pagination_list.forEach(element => {
            pagination_field.innerHTML += `<button onclick="flowPages(${element})">${element}</button>`
        })
        pagination_field.innerHTML += `<button onclick="flowPages(${actual_page+1})">>></button>`
    }
}
const flowPages = (page) => {
    wolves_container.innerHTML=""
    actual_page=page
    let downValue = (page - 1)*4 
    let upperValue = page*4 
    let wolvesInPage = wolvesAll.slice(downValue, upperValue)
    for (let wolf in wolvesInPage) {
        wolves_container.append(createWolfCard($, wolvesInPage[wolf]))
    }
    flowPagination(page)
}

const getWolves = () => {
    fetch(api)
    .then(resp => resp.json())
    .then(resp => {
        wolvesAll = resp
        pages = Math.floor(resp.length / 4)
        flowPages(actual_page);
    })
}
getWolves()

