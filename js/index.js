const $ = document
const container_list = $.querySelector('#list-lobinhos-container')

const getWolves = () => {
    fetch(api)
    .then(resp => resp.json())
    .then(resp => {
        let wolfCard1 = createWolfCard($, resp[0])
        let wolfCard2 = createWolfCard($, resp[1])
        wolfCard1.querySelector(".button-main").remove()
        wolfCard2.querySelector(".button-main").remove()
        container_list.append(wolfCard1)
        container_list.append(wolfCard2)
    })
}

getWolves()


