const $ = document

let wolf_container = $.querySelector("#form-adoption")
let adoption_wolf = $.querySelector("#adoption-wolf-description")
let wolf_id = window.location.href.split("?id=")[1]

fetch(api + wolf_id)
.then(resp => resp.json())
.then(resp => {
    console.log("aaa")
    adoption_wolf.innerHTML=`
        <img src="${resp.link_image}" class="adoption-img">
        <div>
            <h3>${resp.name}</h3>
            <p>Id: ${resp.id}</p>
        </div>
    `
})

$.querySelector("#form-adoption").addEventListener("submit", e=>{
    e.preventDefault()
    let name = $.querySelector("#adoption-name").value
    let age = $.querySelector("#adoption-age").value
    let email = $.querySelector("#adoption-email").value

    let fetchBody = {
        "adoption":{
            "name":name,
            "age":age,
            "email":email,
            "wolf_id":wolf_id
        }
    }
    let fetchConfig = {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(fetchBody)
    }
    fetch(api+"/adoption", fetchConfig)
    .then(resp => resp.json())
    .then(resp => {
        alert("Lobo adotado")
        window.location.href = "list_wolves.html"
    })

})